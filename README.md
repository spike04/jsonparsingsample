# Sample JSON Parsing Using HttpConnection #

Sample JSON Parsing Project using HTTPConnection with the implementation of RequestPackage model.

### What is this repository for? ###

* Performing Request to the Server for Retrieving JSON Data 
* Parsing JSON data
* Model Class Implementation for Data Handling

### How do I get set up? ###

* Clone this Repo and Start parsing JSON

### Who do I talk to? ###

* Repo owner or admin