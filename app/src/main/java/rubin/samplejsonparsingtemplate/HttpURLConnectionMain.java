package rubin.samplejsonparsingtemplate;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by rubin on 11/12/2015.
 * The class returns the required JSON from the URL Passed.
 */
public class HttpURLConnectionMain {

    private static Context context;

    public HttpURLConnectionMain(Context context) {
        this.context = context;
    }

    public static String getData(RequestPackage requestPackage) {
        BufferedReader reader = null;
        String uri = requestPackage.getUri();
        Log.d("Uri", uri);
        if (requestPackage.getMethod().equals("GET")) {
            uri += "?" + requestPackage.getEncodedParams();
        }
        try {
            URL url = new URL(uri);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            if (requestPackage.getMethod().equals("POST") || requestPackage.getMethod().equals("PUT")) {
                con.setDoOutput(true);//thats allows you to output some content to the body of the request
                OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());//can write to the outputstrem and send information to the connection
                writer.write(requestPackage.getEncodedParams());//get value encoded to send to the web
                writer.flush();//that ensures the anything that you written out to memory will flush and send to the server
            }

            con.setConnectTimeout(5000);
            con.setReadTimeout(6000);

            StringBuilder sb = new StringBuilder();
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();

            return "Null Message";
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    return "Null Message";
                }
            }
        }
    }
}