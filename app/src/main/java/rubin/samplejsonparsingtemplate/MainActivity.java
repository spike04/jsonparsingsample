package rubin.samplejsonparsingtemplate;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.List;

import rubin.samplejsonparsingtemplate.model.UserInfo;

public class MainActivity extends AppCompatActivity {

    private static final String URL = "http://api.androidhive.info/contacts/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Sample JSON parsing Code
        RequestPackage requestPackage = new RequestPackage();
        requestPackage.setMethod("GET");
        requestPackage.setUri(URL);

        new GetData().execute(requestPackage); // Here it is required to pass the object or RequestPackage
    }

    class GetData extends AsyncTask<RequestPackage, String, String> {

        @Override
        protected String doInBackground(RequestPackage... params) {
            // Here we are passing the object of request Package
            String result = new HttpURLConnectionMain(getApplicationContext()).getData(params[0]);
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if(!result.equals("Null Message")) {
                // the request was successfull and the data has been obtained in JSON format
                // now parsing and performing required operations

                List<UserInfo> data = Parser.parseData(result);

                // Required operation to display the data. ......

            }
        }
    }
}
