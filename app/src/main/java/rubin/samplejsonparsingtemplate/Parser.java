package rubin.samplejsonparsingtemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import rubin.samplejsonparsingtemplate.model.Phone;
import rubin.samplejsonparsingtemplate.model.UserInfo;

/**
 * Created by rgh on 11/12/2015.
 */
public class Parser {

    // Here the required Parsing of the JSON Data is carried out
    // Sample Example

    static List<UserInfo> data;

    public static List<UserInfo> parseData(String content) {
        data = new ArrayList<>();

        // Required JSON Parsing Steps
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(content);
            JSONArray jsonArray = jsonObject.getJSONArray("meetings");

            for (int i = 0; i < jsonArray.length(); i++) {
                UserInfo userInfo = new UserInfo();

                JSONObject c = jsonArray.getJSONObject(i);

                userInfo.setId(c.getString("id"));
                userInfo.setName(c.getString("name"));
                userInfo.setEmail(c.getString("email"));
                userInfo.setAddress(c.getString("address"));
                userInfo.setGender(c.getString("gender"));

                // Phone node is JSON Object
                Phone phoneNode = new Phone();
                JSONObject phone = c.getJSONObject("phone");
                phoneNode.setMobile(phone.getString("mobile"));
                phoneNode.setHome(phone.getString("home"));
                phoneNode.setOffice(phone.getString("office"));

                userInfo.setPhone(phoneNode);

                data.add(userInfo);
            }

            return data;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

}
